/*
 * Prose spróbowac:
 * - utworzyc kod, w ktorym switch-case bedzie posiadal tylko
 * jedna wywolywana fukcje (trzeba stworzyc dodatkowa funkcje/przepisac juz zrobione)
 * - program powinien byc zapetlony (dzialac az uzytkownik poda wartosc wyjscia)
 * - zobaczyc czym sa obslugi wyjatkow
 * 
 * Wszelkie pytania: piotr_dobosz@int.pl
 */
import java.util.Random;
import java.util.Scanner;

public class LosowanieWlasciwe {
	
	//fukcja zwraca tablice wypelniona liczbami
	static int[] losowanie(int ilosc, int maksimum) {
		//zamieniamy literaly (6,20 itp.) na zmienna przekazana 
		//parametrem (ilosc);dzieki temu funkcja staje sie uniwersalna
		int numery[] = new int[ilosc];
		for (int i=0;i<ilosc;i++) {
			do {
				numery[i]= new Random().nextInt(maksimum)+1;
				for(int j=0;j<i;j++) {
					if (numery[j]==numery[i]) {
						numery[i]=-1;
					}
				}
			}while(numery[i]==-1);
		}
		return numery;
	}
	
	static void wyswietl(String losowanie, int[] n) {
		System.out.print("Twoj zestaw na " + losowanie + ": ");
		for (int i=0;i<n.length;i++) {
			System.out.print(n[i]);
			if (i!=n.length-1)
				System.out.print(", ");
		}
	}
	
	public static void main(String[] args) {
		String przywitanie = "Witaj w programie losowań\n"
				+ "Wybierz jedna z podanych opcji\n\n"
				+ "1.Losowanie lotto\n2.Losowanie multilotek\n"
				+ "3.Losowanie szczesliwy numerek\n4.Ekstra pensja\n"
				+ "5.Wyjscie\n\nTwoj wybor: ";
		System.out.println(przywitanie);
		Scanner wejscie = new Scanner(System.in);
		int wybor = wejscie.nextInt();
		int numery[]; //deklaracja zmiennej tablicowej; pozwala na wykorzystanie nazwy w poszczegolnych przypadkach bez jakichkolwiek bledow
		//gdyz zawsze deklaracja jakiejkolwiek zmiennej MUSI BYC UNIKATOWA w skali pliku programu 
		switch(wybor) {
			case 5: wejscie.close();
				    return;
			case 1: numery = losowanie(6, 49);
					wyswietl("lotto", numery);
					break;
			case 2: numery = losowanie(20, 80);
					wyswietl("multilotka", numery);
					break;
			case 3: int numerek = new Random().nextInt(46)+1;
					numery = losowanie(4, 40);
					System.out.println("Twoj zestaw na szczesliwy numerek: " + numerek); 
					wyswietl("numerki", numery);
					break;
			case 4: numery = losowanie(8, 35);
					wyswietl("ekstra pensje", numery);
					break;
			default: System.out.println("Twoj wybor jest niestandardowy!");
		}
		wejscie.close();
	}
}
