import java.util.Random;
import java.util.Scanner;

public class Podstawy {

	public static void main(String[] args) {
		String przywitanie = "Witaj w programie losowań\n"
				+ "Wybierz jedna z podanych opcji\n\n"
				+ "1.Losowanie lotto\n2.Losowanie multilotek\n"
				+ "3.Losowanie szczesliwy numerek\n4.Ekstra pensja\n"
				+ "5.Wyjscie\n\nTwoj wybor: ";
		System.out.println(przywitanie);
		Scanner wejscie = new Scanner(System.in);
		int wybor = wejscie.nextInt();
		/* ostatni z podstawowych mechanizmow wyborow warunkowych
		 * W klauzuli switch podajemy wartosc, wedle ktorej chcemy by nasz
		 * program reagowal. Wszystkie mozliwe opcje i dzialania powinny 
		 * znalezc sie w nawiasie klamrowym.
		 * W tymze nawiasie mamy obsluge przypadkow (case) gdzie po slowie
		 * kluczowym case pojawia sie wartosc, do KTOREJ WPROWADZONA W 
		 * switch zmienna MA BYC ROWNA (switch-case nie obsluguje innego 
		 * rodzaju porownan). Jezeli taka wartosc sie znajdzie to wykona sie
		 * kod, ktory zostanie zapisany po dwukropku. Nalezy przy tym pamietac,
		 * ze jezeli (ponizszy przypadek) wybor bedzie rowny np. 2, to 
		 * domyslnie warunek wykona sie dla 2 oraz takze dla 3,4 i default.
		 * By temu zapobiec stosuje sie slowa kluczowe break, ktore przerywaja
		 * dzialanie warunku w wybranym przez nas momencie
		 * INFO: return tez przerywa dzialanie case, jednak mozna by ujac,
		 * ze "globalnie" (koczy dzialanie calej funkcji, w ktorej switch-case
		 * sie znajduje).
		 */
		switch(wybor) {
			case 5: return;
			/* ponizej mamy przyklad zmiennej tablicowej. Zmienne te charakteryzuja sie tym,
			 * ze pod jedna, wspolna nazwa mamy mozliwosc zapisania wiecej niz jednej wartosci.
			 * Poszczegolne wartosci sa INDEKSOWANE liczbowo (indeksowane -
			 * przypisywane pod okreslony numer porzadkowy). Zmienne tego 
			 * typu sa niezwykle istotne w programowaniu w przypadku, gdy
			 * potrzebujmy przechowywac duza ilosc danych tego samego typu,
			 * a tworzenie dla nich nowych nazw zmiennych byloby niezwykle
			 * uciazliwe. Ponadto tworzenie unikatowych nazw dla wiekszej
			 * ilosci zmiennych (np. liczba1, liczba2, liczba3 itp. np. do
			 * liczba 1000) jest niepraktyczne - uzupelnianie ich w kodzie
			 * musialoby odbywac sie "recznie" poniewaz nie byloby mozliwosci
			 * odwolac sie do nich tak jak w ponizszej petli
			 */
			case 1: int numery[] = new int[6];//tworzymy tablice o nazwie numery, ktora bedzie miala 6 elementow (wieksza/mniejsza tablice mozna utworzyc podajac odpowiednia wielkosc w nawiasach kwadratowych)
					for (int i=0;i<6;i++) {
						/*tutaj mamy przyklad praktycznego wykorzystania potencjalu tablic;
						 * gdybysmy wykorzystali unikatowe nazwy zmiennych (np. liczba1,liczba2 itp)
						 * to nie moglibysmy wykorzystac petli by uzupelnic wartosci w tychze
						 * zmiennych
						 * nasz kod musialby zawierac, w tym wypadku 6 linii kodu, w ktorych
						 * powielalibysmy instrukcje new Random.nextInt(46)+1
						 * Poniewaz w zmiennych tablicowych do wartosci odwolujemuy sie poprzez
						 * ich indeks (w ponizszym przypadku 0 to indeks pierwszej wartosci,
						 * 5 indeks ostatniej wartosci) caly kod losowania miesci sie w 
						 * jednej linii
						 */
						//numery[i]= new Random().nextInt(46)+1;
						//niestety powyzsze rozwiazanie ma wade - mozliwe
						//jest zduplikowanie wynikow. Dlatego za kazdym 
						//losowaniem trzeba przepatrzec juz wylosowane wyniki
						//czy ktorys z nich nie jest identyczny;
						//jezeli tak losowanie trzeba powtorzyc
						
					}
					//w ponizszym wypadku, poniewaz zmienna numery to tablica,
					//nie zostanie ona wyswietlona tak jak bysmy chcieli (z 
					//szescioma wartosciami); Java wypisze nam jej identyfikator 
					//w pamieci RAM (gdzie znajduja sie nasze wartosci)
					//System.out.println("Twoj zestaw na lotto: " + numery);
					//prawidlo by wyswietlic te dane musimy wykonac petle
					System.out.print("Twoj zestaw na lotto: ");
					for (int i=0;i<6;i++) {
						System.out.print(numery[i]);
						if (i!=5)
							System.out.print(", ");
					}
					break;
			case 2: System.out.println("Twoj zestaw na multilotka: 56,23,1,45,11");
					break;
			//losowanie liczny od 1 do 46
		    //losowanie wartosci w komputerach jest pseudolosowe
		    //czyli przy dluzszych obserwacjach moglibysmy przewidziec wynik losowania
			//dodanie do wyniku losowosci +1 gwarantuje, ze wylosowana liczba nie przyjmie wartosci
			//zerowej oraz jest szansa na wylosowanie 47
			case 3: int numerek = new Random().nextInt(46)+1;
					System.out.println("Twoj zestaw na szczesliwy numerek: " + numerek); 
				    break;
			case 4: System.out.println("Twoj zestaw na ekstra pensje: 78,3,41,1");
					break;
			default: System.out.println("Twoj wybor jest niestandardowy!");
		}
		wejscie.close();
	}

}
