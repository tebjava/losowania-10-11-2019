import java.util.Random;

public class Funkcje {
	/* Ponizej mamy przyklad funkcji losujacej.
	 * Kazda funkcja musi rozpoczynac sie od deklaracji zakresu
	 * widocznosci. Wyjatkiem jest gdy funkcja ma dzialac jedynie
	 * dla naszej klasy bazowej (czym jest klasa bedziemy zajmowac sie
	 * pozniej; na razie wystarczy wiedza, ze klasa jest slowo Podstawy) -
	 * wtedy mozemy pierwsze slowo pominac.
	 * Nastepnie nalezy okreslic czy funkcja bedzie statyczna, czy 
	 * jej wywolanie bedzie mozliwe dopiero po powolaniu do zycia naszej
	 * klasy (obecnie klasa NIE JEST powolywana do zycia). Stad, by 
	 * uzyc naszej funkcji musimy dodac jej slowo kluczowe static (jego
	 * znaczenie bedzie omawiane pozniej)
	 * Kolejnym slowem wymaganym podczas deklaracji funkcji jest tzw.
	 * typ zwracany. Kazda funkcja moze (nie musi) po swoim zakonczeniu
	 * zwracac jakas wartosc. Wartosc te mozna przypisywac np. do 
	 * zmiennych lub wykorzstywac w celach informacyjnych czy funkcja
	 * wykonala sie poprawnie. Ponizej, dla kontrastu funkcji main,
	 * nasza tworzona funkcja bedzie zwracala wartosc boolean (prawda/falsz).
	 * Po powyzszych czynnosciach podajemy nazwe naszej funkcji. Tutaj,
	 * tak samo jak w przypadku nazw zmiennych, mamy swobode w tworzeniu
	 * nazewnictwa (z tymi samymi ograniczeniami). 
	 * Po nazwie funkcji ZAWSZE musza pojawic sie okragle nawiasy. Moga
	 * one nic nie zawierac lecz musza wystapic (bardzo wazne). 
	 * Na koniec uzywamy nawiasow klamrowych; one to pozwalaja nam na
	 * dodawanie rozkazow (instrukcji), ktore maja sie wykonac gdy 
	 * wywolamy nazwe naszej funkcji gdzies w programie (w innej funkcji
	 * badz w funkcji main). Stanowia one tym samym DEFINICJE funkcji
	 */
	static boolean losuj() {
		//losujemy wartosci bez przedzialu
		int los = new Random().nextInt();
		//jezeli wylosowana wartosci jest wieksza od 50 - funkcja
		//nie zwroci prawdy (true) lecz fałsz (false). 
		if (los > 50)
			return false;
		//poniewaz zadeklarowalismy, ze funkcja zwraca wartosc,
		//funkcja MUSI ZAKONCZYC SIE slowem kluczowym return, 
		//po ktorym pojawi sie wartosc true/false; ta wartosc 
		//zostanie zwrocna gdy funkcja zakoczny swoje dzialanie
		return true;
	}
	
	
	/*To jest przyklad funkcji; jak sama wskazuje funkcje maja swoja
	 * okreslona funkcjonalnosc; main jest szczegolnym przypadkiem bo tego typu 
	 * funkcja wywolywana jest przez sam system operacyjny. Kazdy system
	 * operacyjny przy uruchamianiu napisanej przez nas aplikacji
	 * szuka wlasnie funkcji main; Jezeli ja znajdzie wykonuje wszystkie
	 * instrukcje w niej zawarte i konczy program. Poniewaz ta funkcja
	 * jest wywolywana przez JVM (maszyne wirtualna Java), w przeciwiestwie
	 * do innych jezykow nic nie zwraca (typ void) oraz wymaga
	 * podania (nawet pustych) argumentow startowych programu, ktore
	 * wedruja do zmiennej args
	 */
	public static void main(String[] args) {
		/* wywolujemy wczesniej zadelarowana przez nas funkcje;
		 * poniewaz nastepuje w niej losowanie bez przedzialu, brane sa
		 * pod uwage wyniki od 0  do ok 4 mld wartosci. Pamietajac z 
		 * definicji kodu funkcji, jezeli wylosujemy wartosci ponizej 51
		 * to znalezlismy sie w zakresie losowania, jezeli nie - funkcja
		 * zwraca wartosc false i dostajemy stosowany komunikat.
		 */
		if (losuj()) {
			System.out.println("Wylosowana liczba jest w przedziale 0-50");
		}
		else {
			System.out.println("Liczba poza zakresem");
		}
		
	}
}
