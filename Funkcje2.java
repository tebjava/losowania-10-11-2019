import java.util.Random;

public class Podstawy {
	/* W poprzednim przykladzie funkcja po prostu byla wywolywana.
	 * Teraz jednak chcielibysmy by programista mogl do naszej funkcji
	 * wprowadzic jakas wlasna wartosc - np. wartosc graniczna przedzialu,
	 * z ktorego chcemy pozyskiwac wartosci. Do tego sluza tzw. parametry
	 * funkcji. Parametry to nic innego jak zminne, ktore deklaruje sie nie
	 * w ciele funkcji (jak np. zmienna los) tylko w okraglych nawiasach
	 * wystepujacych po nazwie funkcji.
	 * Ponizej utwrzona zostala zmienna-parametr wskazujaca zakres (max),
	 * dla ktorego nasza funkcja bedzie zwracac wartosc true
	 * Funkcje moga miec wiecej niz jeden parametr
	 */
	static boolean losuj(int maksimum) {
		int los = new Random().nextInt(); 
		//zamieniono literal na zmienna przekazana do funkcji jako parametr
		if (los > maksimum)
			return false;
		return true;
	}
	/*Przyklad funkcji z wieksza iloscia parametrow, bez zwracania wartosci
	 */
	static void losuj2(int minimum, int maksimum) {
		int los=new Random().nextInt();
		if (los > minimum && los < maksimum) {
			System.out.println("Wylosowano liczbe z zakresu " + minimum + " " + maksimum);
		}
		else {
			System.out.print(los + " ");
			System.out.println("Liczba poza zakresem " + minimum + " " + maksimum);
		}
		//slowo moze sie pojawic jednak w przypadku typu void (pustka)
		//nie jest ono w ogole wymagane bo i tak nic nie zwroci
		//return;	
	}
	
	static int losPrzedzial(int minimum, int maksimum) {
		//np. minimum to 15, maksimum 45
		//w tym wypadku w nextInttrzeba podac wartosc maksymalnego losowania
		//pomniejszona o wartosc minimalna (czyli 5-15 = 30). Losujemy
		//zakres 0-29. 
		//Nastepnie do wyniku dodajemy nasze minimum, co pozwala nam
		//na pozyskanie wyniku z przedzialu 15-44 (29+15=44). Dlatego też
		//kazdorazowo dodajemy wartosc 1 by dopelnic nasz zakres do
		//wartosci oczekiwanej;
		//int los=new Random().nextInt(maksimum-minimum)+minimum+1;
		//jezeli chcemy losowac liczby z domknietetego zakresu (zawierajacego
		//minimum i maksimu wlacznie) miusimy przebudowac nasz "alogorytm"
		//zwiekszamy zakres losowania o 1 (45-15+1= 31) czyli losowanie bedzie
		//z zakresu 0-30 (bez 31). Dodajc minimum (30+15) otrzymamy 45, jednak
		//przy wolosowaniu 0 otrzymamy 15 (a nie 16, jak w poprzednim przypadku)
		int los=new Random().nextInt(maksimum-minimum+1)+minimum;
		//funkcja bedzie zwracac wylosowana liczbe - utworzona zmienna
		return los;
	}
	
	public static void main(String[] args) {
		/* od tego momentu nasza funkcja musi miec przekazany przez nas
		 * argument; jezeli tego nie zrobimy (nie wpiszemy go w nawias)
		 * to niestety bedzie to wywolywac blad kodu; poniewaz parametr
		 * ma tym int, mozemy podac dowolna liczbe z tego zakresu
		 */
		if (losuj(1000)) {
			System.out.println("Wylosowana liczba jest w przedziale 0-1000");
		}
		else {
			System.out.println("Liczba poza zakresem");
		}
		//zakresy moga sie pokryc, moga tez byc rozne; nalezy pamietac,
		//ze kazde wywolanie funkcji losuj to nowe losowanie (nowe wartosci!)
		if (losuj(10000)) {
			System.out.println("Wylosowana liczba jest w przedziale 0-10000");
		}
		else {
			System.out.println("Liczba poza zakresem");
		}
		//przyklad wywolania funkcji losuj2; musimy podac dwa arugmenty
		losuj2(15,100);
		losuj2(200,1000);
		losuj2(12,15);
		losuj2(1000,100000);
		
		//dzialanie funkji los przedzial - zapisanie 10 losowan poprzez
		//petle for
		int losowane[] = new int[10];
		for (int i=0;i<10;i++) {
			losowane[i] = losPrzedzial(10, 60);
		}
		//po wylosowaniu sprawdzamy dzialanie naszej funkcji:
		for (int i=0;i<10;i++) {
			System.out.print(losowane[i] + ", ");
		}
	}
}
