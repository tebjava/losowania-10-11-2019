/* Ponizszy kod jest przykladem blednego powielania intrukcji
 * w poszczegolnych warunkach programu. Tego typu praktyka jest niewskazana:
 * - nadmiarowy kod staje sie nieczytelny
 * - kod blednie skopiowany musi byc poprawiany razy ilosc kopii
 * - plik kodu nienaturalnie sie rozrasta stajac sie nieefektywnym
 * - kazde detale (np. zmiana pojedynczej wartosci) powoduje 
 * koniecznosc zmian w reszcie kodu
 * 
 * Eliminacja tego zjawiska sa funkcje, ktore beda opisane w kolejnym
 * pliku projektu
 */

import java.util.Random;
import java.util.Scanner;

public class Podstawy {
	
	public static void main(String[] args) {
		String przywitanie = "Witaj w programie losowań\n"
				+ "Wybierz jedna z podanych opcji\n\n"
				+ "1.Losowanie lotto\n2.Losowanie multilotek\n"
				+ "3.Losowanie szczesliwy numerek\n4.Ekstra pensja\n"
				+ "5.Wyjscie\n\nTwoj wybor: ";
		System.out.println(przywitanie);
		Scanner wejscie = new Scanner(System.in);
		int wybor = wejscie.nextInt();
		int numery[]; //deklaracja zmiennej tablicowej; pozwala na wykorzystanie nazwy w poszczegolnych przypadkach bez jakichkolwiek bledow
		//gdyz zawsze deklaracja jakiejkolwiek zmiennej MUSI BYC UNIKATOWA w skali pliku programu 
		switch(wybor) {
			case 5: wejscie.close();
				    return;
			case 1: numery = new int[6];//uzywamy wczesniejszej deklaracji do utworzenia definicji naszej tablicy (tworzymy 6 elementow)
					for (int i=0;i<6;i++) {
						do {
							numery[i]= new Random().nextInt(46)+1;
							for(int j=0;j<i;j++) {
								if (numery[j]==numery[i]) {
									numery[i]=-1;
								}
							}
						}while(numery[i]==-1);
					}
					System.out.print("Twoj zestaw na lotto: ");
					for (int i=0;i<6;i++) {
						System.out.print(numery[i]);
						if (i!=5)
							System.out.print(", ");
					}
					break;
			case 2: numery = new int[20]; //jw. ale tworzymy 20 elementow
					for (int i=0;i<20;i++) {
						do {
							numery[i]= new Random().nextInt(79)+1;
							for(int j=0;j<i;j++) {
								if (numery[j]==numery[i]) {
									numery[i]=-1;
								}
							}
						}while(numery[i]==-1);
					}
					System.out.print("Twoj zestaw na multilotka: ");
					for (int i=0;i<20;i++) {
						System.out.print(numery[i]);
						if (i!=19)
							System.out.print(", ");
					}
					break;
			case 3: int numerek = new Random().nextInt(46)+1;
					numery = new int[4]; //jw. ale tworzymy 20 elementow
					for (int i=0;i<4;i++) {
						do {
							numery[i]= new Random().nextInt(40)+1;
							for(int j=0;j<i;j++) {
								if (numery[j]==numery[i]) {
									numery[i]=-1;
								}
							}
						}while(numery[i]==-1);
					}
					System.out.println("Twoj zestaw na szczesliwy numerek: " + numerek); 
					System.out.print("Twoj zestaw numerkow: "); 
					for (int i=0;i<4;i++) {
						System.out.print(numery[i]);
						if (i!=3)
							System.out.print(", ");
					}
					break;
			case 4: System.out.println("Twoj zestaw na ekstra pensje: 78,3,41,1");
					break;
			default: System.out.println("Twoj wybor jest niestandardowy!");
		}
		wejscie.close();
	}
}
